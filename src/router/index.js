import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import RoomPage from "../views/RoomPage.vue"

const routes = [
  {
    path: "/",
    name: 'Home',
    component: Home
  },
  {
    path:"/RoomPage",
    name: "RoomPage",
    component: RoomPage
  }
  
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  scrollBehavior(){
      document.getElementById("app").scrollIntoView();
  }
})

export default router
